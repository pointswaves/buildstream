#
# Tox global configuration
#
[tox]
envlist = py{35,36,37}
skip_missing_interpreters = true

#
# Defaults for all environments
#
# Anything specified here is inherited by the sections
#
[testenv]
commands =
    # Running with coverage reporting enabled
    py{35,36,37}-!external-!nocover: pytest --basetemp {envtmpdir} --cov=buildstream --cov-config .coveragerc {posargs}
    py{35,36,37}-!nocover: mkdir -p .coverage-reports
    external-!nocover: {envpython} {toxinidir}/tests/external_plugins.py {envtmpdir} --cov=buildstream --cov-append {posargs}
    py{35,36,37}-!nocover: mv {envtmpdir}/.coverage {toxinidir}/.coverage-reports/.coverage.{env:COVERAGE_PREFIX:}{envname}
    # Running with coverage reporting disabled
    py{35,36,37}-!external-nocover: pytest --basetemp {envtmpdir} {posargs}
    external-nocover: {envpython} {toxinidir}/tests/external_plugins.py {envtmpdir} {posargs}
deps =
    py{35,36,37}: -rrequirements/requirements.txt
    py{35,36,37}: -rrequirements/dev-requirements.txt
    py{35,36,37}: -rrequirements/plugin-requirements.txt

    # Only require coverage and pytest-cov when using it
    !nocover: -rrequirements/cov-requirements.txt
passenv =
    BST_FORCE_BACKEND
    GI_TYPELIB_PATH
    INTEGRATION_CACHE
    http_proxy
    HTTP_PROXY
    https_proxy
    HTTPS_PROXY
    no_proxy
    NO_PROXY
    SSL_CERT_FILE

#
# These keys are not inherited by any other sections
#
setenv =
    py{35,36,37}: COVERAGE_FILE = {envtmpdir}/.coverage
whitelist_externals =
    py{35,36,37}:
        mv
        mkdir

#
# Coverage reporting
#
[testenv:coverage]
commands =
    - coverage combine --rcfile={toxinidir}/.coveragerc {toxinidir}/.coverage-reports/
    coverage report --rcfile={toxinidir}/.coveragerc -m
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/cov-requirements.txt
setenv =
    COVERAGE_FILE = {toxinidir}/.coverage-reports/.coverage

#
# Running linters
#
[testenv:lint]
commands =
    pycodestyle
    pylint buildstream
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt


#
# Building documentation
#
[testenv:docs]
commands =
    make -C doc
# sphinx_rtd_theme < 0.4.2 breaks search functionality for Sphinx >= 1.8
deps =
    sphinx
    sphinx-click
    sphinx_rtd_theme >= 0.4.2
    pytest
    -rrequirements/requirements.txt
    -rrequirements/plugin-requirements.txt
passenv =
    BST_FORCE_SESSION_REBUILD
    BST_SOURCE_CACHE
    HOME
    LANG
    LC_ALL
whitelist_externals =
    make

#
# (re-)Generating man pages
#
[testenv:man]
commands =
    python3 setup.py --command-packages=click_man.commands man_pages
deps =
    click-man >= 0.3.0
    -rrequirements/requirements.txt

#
# Usefull for running arbitrary scripts in a BuildStream virtual env
#
[testenv:venv]
commands = {posargs}
deps =
    -rrequirements/requirements.txt
    -rrequirements/dev-requirements.txt
    -rrequirements/plugin-requirements.txt
whitelist_externals = *
